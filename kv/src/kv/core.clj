(ns kv.core
  (:require [compojure.core :refer :all]
            [compojure.route :as route]
            [taoensso.carmine :as car :refer (wcar)]))

(def server1-conn {:pool {} :spec {:host "localhost" :port 6379}})
(defmacro wcar* [& body] `(car/wcar server1-conn ~@body))

(defn get-value [{{key :key} :params}]
  (wcar* (car/get key)))

(defn set-value [{{key :key} :params body :body}]
  (let [doc (slurp body)]
    (wcar* (car/set key doc))))

(defroutes app
  (GET "/:key" request (get-value request))
  (POST "/:key" request (set-value request)))
