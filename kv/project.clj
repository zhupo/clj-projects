(defproject kv "0.1.0-SNAPSHOT"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.8.0"]
                 [compojure "1.5.2"]
                 [com.taoensso/carmine "2.15.1"]]
  :plugins [[lein-ring "0.8.10"]]
  :ring {:handler kv.core/app}
)
