(ns random-web.generators-test
  (:require [clojure.test :refer :all]
            [random-web.generators :refer :all]))

(deftest test-rand-str
  (testing "Testing return type and length of rand-str"
    (is (= (count (rand-str 5)) 
           5))
    (is (= (count (rand-str)) 
           8))
    (is (instance? java.lang.String (rand-str)))
))
