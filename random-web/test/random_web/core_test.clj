(ns random-web.core-test
  (:require [clojure.test :refer :all]
            [random-web.core :refer :all]))

(deftest test-observe
  (testing "Testing keys and types for map returned by (observe)"
    (let [obs (observe)]
      (is (contains? obs :timestamp))
      (is (contains? obs :value))
      (is (instance? java.util.Date (:timestamp obs)))
      (is (instance? java.lang.Double (:value obs)))
)))

(deftest test-randomness
  (testing "Testing that two observations won't have equal values"
    (let [obs1 (observe)]
      (Thread/sleep 100)
      (let [obs2 (observe)]
        (not (= (:value obs1) 
                (:value obs2)))
        (is (= (compare (:timestamp obs1) (:timestamp obs2))
               -1))
))))

