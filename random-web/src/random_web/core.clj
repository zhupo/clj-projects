(ns random-web.core
  (:require [compojure.core :as compojure]
            [ring.adapter.jetty :as jetty]
            [cheshire.core :as cheshire]))

(defn observe [] 
  {:timestamp (java.util.Date.) 
   :value (rand)})


(def handler
  (compojure/routes
    (compojure/GET "/:n" [n] 
      (str 
        (cheshire/generate-string 
          (repeatedly (read-string n) observe) 
          {:pretty true})
        "\n"))))

(defn -main []
  (jetty/run-jetty
    handler
    {:port 3000 :join? false}))
