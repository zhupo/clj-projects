(ns random-web.generators)

(defn rand-str
  "Returns a random alphanumeric string of length n (default 8)."
  ([] (rand-str 8))
  ([n] (let [codes (concat (range 48 58)    ; digits 
                           (range 65 91)    ; uppercase letters
                           (range 97 123))] ; lowercase letters
         (apply str
           (map (comp str char)
             (repeatedly n #(rand-nth codes)))))))

